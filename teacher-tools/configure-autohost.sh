#!/bin/bash -eu
ssh-add -l | grep dlwillson-yoga-2019-05 || source sshagentrc
ssh fedora@fuga.sofree.us sudo dnf -y install python libselinux-python nginx
ansible-playbook ${0/.sh/.yml} -u fedora
