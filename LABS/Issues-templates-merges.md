1. Import, clone, or fork this project or another recipe project or create a new recipe project.
   * If you choose to fork this project, break the fork relationship: Settings, General, Advanced, Remove Fork Relationship
   * In Settings > Members, add your partner as a Developer

2. In your partner's project, create an Issue:
  * Issue suggestions:
    - add/change/delete an ingredient
    - improve formatting or phrasing
    - add a new recipe

3. From the Issue, create and submit a Merge Request
  * Click "Create merge request"
  * Code up your proposed solution to the Issue.
  * Clear the 'WIP' status on the MR and assign it to your partner when you are satisfied with your proposal.

---

In your project:
- Accept, fix, or reject your partner's proposed merge request.

---

In your project:
- Create an Issue Template. [examples](/.gitlab/issue_templates/)
- Optionally, change the default Issue Template - see https://docs.gitlab.com/ee/user/project/description_templates.html 

---

In your partner's project:
- Create an Issue using one of their Issue Templates.
